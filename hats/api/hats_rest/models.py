from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=100, unique=True, null=True)

class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=700)
    location = models.ForeignKey(
        LocationVO,
        on_delete=models.CASCADE,
        related_name="locations",
    )

    def get_api_url(self):
        return reverse("api_hat", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.style}: {self.fabric}, {self.color}"

    class Meta:
        ordering = ("fabric", "style", "color", "picture_url", "location")
