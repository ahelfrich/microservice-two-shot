from django.db import models
from django.urls import reverse

# Create your models here.


class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True, null=True)


class Shoe(models.Model):
    model_name = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        on_delete=models.CASCADE,
        related_name='shoes',
        null=True,
    )

    # def get_api_url(self):
    #     return reverse("api_location", kwargs={"pk": self.pk})

    # def __str__(self):
    #     return f"{self.model_name} - {self.manufacturer} in {self.color}"

    # class Meta:
    #     ordering = ("model_name", "manufacturer", "color", "url")