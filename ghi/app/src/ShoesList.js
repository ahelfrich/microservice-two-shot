function ShoesList(props) {
  console.log(props, "this is props")
  const handleDeleteShoe = async (id) => {
    const shoeUrl = `http://localhost:8080/api/shoes/${id}`;
    const response = await fetch(shoeUrl, { method: "DELETE" });
    if (response.ok) {
      props.shoes.filter((shoe) => shoe.id !== id);
      window.location.replace("/shoes")
    }
  };

    return (
      <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Photo</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes?.map((shoe) => {
            return (
              <tr key={shoe.id}>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td className="w-25">
                  <img
                    className="img-fluid img-thumbnail"
                    src={shoe.url}
                    width="100"
                    height="100"
                  />
              </td>
              <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => handleDeleteShoe(shoe.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    <a href={'/shoes/new'} className="btn btn-primary btn-lg active" role="button" aria-pressed="true">Create Shoe</a>
    </div>
    );
  }

  export default ShoesList;
