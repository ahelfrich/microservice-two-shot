function Hats(props) {
  const handleDeleteHat = async (id) => {
    const hatUrl = `http://localhost:8090/api/hats/${id}`;
    const response = await fetch(hatUrl, { method: "DELETE" });
    if (response.ok) {
      props.hats.filter((hat) => hat.id !== id);
      window.location.reload();
    }
  };

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Style</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Photo</th>
          </tr>
        </thead>
        <tbody>
          {props.hats?.map((hat) => {
            return (
              <tr key={hat.id}>
                <td>{hat.style}</td>
                <td>{hat.fabric}</td>
                <td>{hat.color}</td>
                <td className="w-25">
                  <img
                    className="img-fluid img-thumbnail"
                    src={hat.picture_url}
                    width="100"
                    height="100"
                  />
                </td>
                <td>
                  <button
                    onClick={() => handleDeleteHat(hat.id)}
                    className="btn btn-info"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <a href={"hats/new/"} className="btn btn-info" role="button">
        Create Hat
      </a>
    </div>
  );
}

export default Hats;
