import React, {useEffect, useState } from 'react';

export default function NewShoeForm() {
  const [bins, setBins] = useState([])
  const [modelName, setModelName] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [url, setUrl] = useState('');
  const [bin, setBin] = useState('');
  
  const handleModelName = async (event) => {
    const value = event.target.value;
    setModelName(value);
  }

  const handleManufacturer = async (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

  const handleUrl = async (event) => {
    const value = event.target.value;
    setUrl(value);
  }

  const handleBin = async (event) => {
    const value = event.target.value;
    setBin(value);
  }

  const handleSubmit = async (event) =>{
    event.preventDefault()

    const data = {
      model_name: modelName,
      manufacturer,
      url,
      bin: bin,
    }
  

    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json",
      }
    };

    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe)
      setModelName("")
      setManufacturer("")
      setUrl("")
      setBin([])
      window.location.replace("/shoes")
    }
  }

  const fetchData = async () =>{
    const url = "http://localhost:8100/api/bins";
    const response = await fetch(url);
    if (response.ok){
      const data = await response.json();
      setBins(data.bins);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return(
      <div className="my-5 container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new shoe</h1>
              <form onSubmit={handleSubmit} id="crate-shoe-form">
                <div className="form-floating mb-3">
                  <input
                  value={modelName}
                  onChange={handleModelName}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                  />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                  value={manufacturer}
                  onChange={handleManufacturer}
                  placeholder="manufacturer"
                  required
                  type="text"
                  name="manufacturer"
                  id="manufacturer"
                  className="form-control"
                  />
                  <label htmlFor="manufacturer">manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                  value={url}
                  onChange={handleUrl}
                  placeholder="url"
                  required
                  type="text"
                  name="url"
                  id="url"
                  className="form-control"
                  />
                  <label htmlFor="url">url</label>
                </div>
                <div className="mb-3">
                  <select
                  value={bin}
                  onChange={handleBin}
                  required
                  name = "bin"
                  id="bin"
                  className="form-select"
                >
                  <option value="">Choose a bin</option>
                  {bins.map((bin) =>{
                    return (
                      <option key={bin.id} value={bin.id}>
                        {bin.closet_name}
                      </option>
                    );
                  })}
                  </select>
                </div>
                <button className="btn btn-primate">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
  );
      }
