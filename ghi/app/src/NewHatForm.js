import React, { useEffect, useState } from "react";

export default function NewHatForm() {
  const [locations, setLocations] = useState([]);
  const [fabric, setFabric] = useState("");
  const [style, setStyle] = useState("");
  const [color, setColor] = useState("");
  const [pictureUrl, setPictureUrl] = useState("");
  const [location, setLocation] = useState("");

  const handleFabricChange = async (event) => {
    const value = event.target.value;
    setFabric(value);
  };

  const handleStyleChange = async (event) => {
    const value = event.target.value;
    setStyle(value);
  };

  const handleColorChange = async (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handlePictureUrlChange = async (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  };

  const handleLocationChange = async (event) => {
    const value = event.target.value;
    setLocation(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      fabric: fabric,
      style: style,
      color: color,
      picture_url: pictureUrl,
      location: location,
    };

    const hatUrl = "http://localhost:8090/api/hats/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json",
      },
    };

    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);
      setFabric("");
      setStyle("");
      setColor("");
      setPictureUrl("");
      setLocation([]);
      window.location.replace("/hats");
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8100/api/locations/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input
                  value={style}
                  onChange={handleStyleChange}
                  placeholder="Style"
                  required
                  type="text"
                  name="style"
                  id="style"
                  className="form-control"
                />
                <label htmlFor="name">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={fabric}
                  onChange={handleFabricChange}
                  placeholder="Fabric"
                  required
                  type="text"
                  name="fabric"
                  id="fabric"
                  className="form-control"
                />
                <label htmlFor="manufacturer">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={color}
                  onChange={handleColorChange}
                  placeholder="Color"
                  required
                  type="text"
                  name="color"
                  id="color"
                  className="form-control"
                />
                <label htmlFor="url">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={pictureUrl}
                  onChange={handlePictureUrlChange}
                  placeholder="Picture URL"
                  required
                  type="text"
                  name="picture url"
                  id="picture url"
                  className="form-control"
                />
                <label htmlFor="url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select
                  value={location}
                  onChange={handleLocationChange}
                  required
                  name="location"
                  id="location"
                  className="form-select"
                >
                  <option value="">Choose a location</option>
                  {locations.map((location) => {
                    return (
                      <option key={location.id} value={location.id}>
                        {location.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-info">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

// function NewHatForm() {
//     return (
//         <div className="row">
//         <div className="offset-3 col-6">
//           <div className="shadow p-4 mt-4">
//             <h1>Create a new hat</h1>
//             <form onSubmit={handleSubmit} id="create-presentation-form">
//               <div className="form-floating mb-3">
//                 <input
//                   placeholder="Fabric"
//                   required
//                   type="text"
//                   name="fabric"
//                   id="fabric"
//                   className="form-control"
//                   value={fabric}
//                 />
//                 <label htmlFor="presenter_name">Fabric</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input
//                   placeholder="style"
//                   required
//                   type="text"
//                   name="style"
//                   id="style"
//                   className="form-control"
//                   value={presenterEmail}
//                 />
//                 <label htmlFor="presenter_email">Presenter email</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input
//                   onChange={handleCompanyNameChange}
//                   placeholder="Company name"
//                   type="text"
//                   name="company_name"
//                   id="company_name"
//                   className="form-control"
//                   value={companyName}
//                 />
//                 <label htmlFor="company_name">Company name</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input
//                   onChange={handleTitleChange}
//                   placeholder="Title"
//                   required
//                   type="text"
//                   name="title"
//                   id="title"
//                   className="form-control"
//                   value={title}
//                 />
//                 <label htmlFor="title">Title</label>
//               </div>
//               <div className="mb-3">
//                 <label htmlFor="synopsis">Synopsis</label>
//                 <textarea
//                   onChange={handleSynopsisChange}
//                   className="form-control"
//                   id="synopsis"
//                   rows="3"
//                   name="synopsis"
//                   value={synopsis}
//                 ></textarea>
//               </div>
//               <div className="mb-3">
//                 <select
//                   onChange={handleConferenceChange}
//                   required
//                   name="conference"
//                   id="conference"
//                   className="form-select"
//                   value={conference}
//                 >
//                   <option value="">Choose a conference</option>
//                   {states.map((conference) => {
//                     return (
//                       <option key={conference.href} value={conference.href}>
//                         {conference.name}
//                       </option>
//                     );
//                   })}
//                 </select>
//               </div>
//               <button className="btn btn-primary">Create</button>
//             </form>
//           </div>
//         </div>
//       </div>
//     );
// }
