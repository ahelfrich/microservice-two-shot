# Wardrobify

Team:

- Adrianna Helfrich - Hats
- Justin Burgoino - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

I started in my my models and created a Shoe model and a BinVo model with my foreign key in my shoe class that connects to my BinVo which then connects to the wardrope API bins through poller.py. Once I was able to make shoes, delete shoes, and get shoe list I then worked on the front end. Using forms such as shoesList and NewShoeForm I was able to connect the pages and have enough functionality to where the user is able to create a shoe, delete a shoe and get the list of shoes that they made.

Explain your models and integration with the wardrobe
microservice, here.

My models.py contains a Hat model as well as a LocationVO model. Within the Hat class, I’ve added all the required fields for each hat’s details, including the location in which they are located. The location field is a Foreign Key which links to the Location model in the Wardrobe microservice by use of the LocationVO model. This brings in the fields associated with the Location model in Wardrobe and allows me to use them as a value object within my Hat model. I also have a poller implemented to retrieve data from the Wardrobe microservice in order to access and utilize information for the locations.
